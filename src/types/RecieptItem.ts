import type { Product } from "./Product"

type Receiptitem ={
    id:number
    name:string
    price:number
    unit:number
    ProductId:number
    product?:Product
}
export{type Receiptitem}