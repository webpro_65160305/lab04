import type { Member } from "./Member";
import type { Receiptitem } from "./RecieptItem";
import type { User } from "./User";

type Receipt={
    id:number;
    createdData:Date;
    totalBefore:number;
    total:number;
    memberDiscount:number;
    receivedAmount:number;
    change:number;
    paymentType:string;
    userId:number;
    user?:User;
    memberId:number;
    member?:Member;
    receiptItem?:Receiptitem[]
}
export type {Receipt}